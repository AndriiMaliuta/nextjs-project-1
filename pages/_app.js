import Layout from '../components/Layout';
import '../styles/global.css';
import { UserProvider } from '@auth0/nextjs-auth0';

function MyApp({ Component, pageProps }) {
  return (
    <UserProvider>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </UserProvider>
  );
}

// export function reportWebVitals(metric) {
//   console.log(metric);
// }

export default MyApp;
