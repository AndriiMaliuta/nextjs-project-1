import {
  withApiAuthRequired,
  getSession,
  getAccessToken,
} from '@auth0/nextjs-auth0';

export default withApiAuthRequired(async function handler(req, res) {
  const session = getSession(req, res);
  const userId = session.user.sub;
  console.log(userId);
  // req.setHeader('test', 'TEST HEADER');
  const { accessToken } = await getAccessToken(req, res);
  res.statusCode = 200;
  res.json({ name: 'Murchyk' });
});
