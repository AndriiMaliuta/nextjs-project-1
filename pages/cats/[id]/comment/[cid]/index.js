import { useRouter } from 'next/router';
import Link from 'next/link';

const comment = () => {
  const router = useRouter();
  console.log(router.query);
  const { cid } = router.query;
  return (
    <div>
      <h2>Comment</h2>
      <p>{cid}</p>
    </div>
  );
};

export default comment;
