const Cat = ({ id }) => {
  return (
    <div>
      <h2>Cat Page</h2>
      <p>{id}</p>
    </div>
  );
};

export default Cat;

export async function getStaticProps(context) {
  // context = {
  //   params: { id: '2' },
  //   locales: undefined,
  //   locale: undefined,
  //   defaultLocale: undefined,
  // };
  console.log(context);
  return { props: { id: context.params.id } };
}

export async function getStaticPaths(context) {
  return {
    paths: [{ params: { id: '1' } }, { params: { id: '2' } }],
    fallback: false,
  };
}
