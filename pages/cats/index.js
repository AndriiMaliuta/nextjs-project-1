import { withPageAuthRequired } from '@auth0/nextjs-auth0';

const Cats = ({ data }) => {
  return (
    <div>
      <h2>Cats Page</h2>
      <p>{data}</p>
    </div>
  );
};

export async function getStaticProps(context) {
  return { props: { data: 'test 2' } };
}

export default withPageAuthRequired(Cats);
