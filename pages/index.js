import Head from 'next/head';
import { Fragment } from 'react';
import { useUser } from '@auth0/nextjs-auth0';
import { withPageAuthRequired } from '@auth0/nextjs-auth0';

function Home() {
  // console.log(process.env.DB_HOST);

  const { user, error, isLoading } = useUser();

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>{error.message}</div>;

  return (
    <Fragment>
      <Head>
        <title>Auth0 Cats App</title>
      </Head>
      <div className='container'>
        <h2>Cats App</h2>
        <div>Cats site</div>
        {user && <div>{user.name}</div>}
      </div>
    </Fragment>
  );
}

export default withPageAuthRequired(Home, {
  onRedirecting: () => <div>Loading...</div>,
  onError: (error) => <div>{error.message}</div>,
});

export const getServerSideProps = withPageAuthRequired();
