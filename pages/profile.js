import React from 'react';
import { useUser, withPageAuthRequired } from '@auth0/nextjs-auth0';

// import Loading from '../components/Loading';
// import ErrorMessage from '../components/ErrorMessage';
// import Highlight from '../components/Highlight';

function Profile() {
  const { user, isLoading } = useUser();

  return (
    <>
      {isLoading && <div>Loading...</div>}
      {user && (
        <>
          <h2 data-testid='profile-name'>{user.name}</h2>
          <p className='lead text-muted' data-testid='profile-email'>
            {user.email}
          </p>
        </>
      )}
    </>
  );
}

export default withPageAuthRequired(Profile, {
  onRedirecting: () => <div>Loading...</div>,
  onError: (error) => <div>{error.message}</div>,
});
